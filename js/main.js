document.addEventListener('DOMContentLoaded', () =>{
    class Tour{
        constructor(element, tourPrice, countryPrice , hotelPrice, 
                    levelPrice, typeTransp, numDays, numPers, numChildren){
            this.$element = element
            this.$tourPrice = tourPrice
            this.$countryPrice = countryPrice
            this.$hotelPrice = hotelPrice
            this.$levelPrice = levelPrice
            this.$typeTransp = typeTransp
            this.$numDays = numDays
            this.$numPers = numPers
            this.$numChildren = numChildren

            let boxPrice = this.$element.querySelector('#boxPrice')
            let boxPers = this.$element.querySelector('#boxPers')
            boxPers.innerHTML = ''
            boxPrice.innerHTML = ''
            let price, totalPrice, html
            if(this.$numDays >= 10) {
                price = parseFloat(this.$tourPrice*this.$countryPrice*this.$hotelPrice*this.$levelPrice)*0.2
                html = `<p>У вас 20% скидка, так как длительность тура больше 10 дней</p>
                        <p>Вы выбрали ${4}-звездочный отель, 
                        цена за 1 день составляет : ${price}</p>
                        <p>Цена на ${this.$numDays} дней: ${price*this.$numDays}</p>
                        
                        <p></p>
                        <p></p>`
                totalPrice = 0
            } else {
                price = parseFloat(this.$tourPrice*this.$countryPrice*this.$hotelPrice*this.$levelPrice)
                html = `<p>У вас нет скидки, так как длительность тура меньше 10 дней</p>
                        <p>Вы выбрали ${4}-звездочный отель, 
                        цена за 1 день составляет: ${price}</p>
                        <p>Цена на ${this.$numDays} дней: ${price*this.$numDays}</p>
                        
                        <p></p>
                        <p></p>`
                        debugger
                totalPrice = 1
            }

           
            const htmlT = `<p>Оканчательная цена за тур: ${totalPrice}</p>` 
            boxPers.insertAdjacentHTML('afterbegin', html)
            boxPrice.insertAdjacentHTML('afterbegin', htmlT)
        }
        open(){
            this.$element.style.display = 'block'
        }

    }

    constructorSelect('http://localhost:3000/tours', '#inputState')
    constructorSelect('http://localhost:3000/countrys', '#inputCountry')
    constructorSelect('http://localhost:3000/hotels', '#inputHotel')
            
    document.querySelector('#but').addEventListener('click', () => {
        let element = document.querySelector('#rezultat')
        
        const tourPrice = getValue('#inputState')
        const countryPrice = getValue('#inputCountry')
        const hotelPrice = getValue('#inputHotel')
        const levelPrice = getValue("#inputLevel")
        const typeTransp = getValue("#inputTr")
        const numDays = document.querySelector('#numNigths').value
        const numPers = document.querySelector('#numPers').value
        const numChildren = document.querySelector('#numChild').value
        
        let tourPerson = new Tour(element, tourPrice, countryPrice, hotelPrice,
            levelPrice, typeTransp, numDays, numPers, numChildren)
        tourPerson.open()
        
    })

//Все функции
    //====Функция fetch которая  строит в select
    function constructorSelect(url, id) {
        fetch(url)
            .then(response =>{
                return response.json()
            })
            .then(data => {
                var input = document.querySelector(id)
                var dataJSON = data.map(function(item){
                    return `<option value="${item.price}">${item.name}</option>`
                })

                input.insertAdjacentHTML('beforeend', dataJSON.join(' '))
            })
    }

    //===== Функция которая берет выбранный элемент в select
    function getValue(id){
        let select = document.querySelector(id)
        let price = select.options[select.selectedIndex].value
        return price
    }
})